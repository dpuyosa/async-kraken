pub const API_URL: &str = "https://api.kraken.com";
pub const API_VER: &str = "0";

const PUBLIC_METHODS: &'static [&'static str] = &[
    "Time",
    "SystemStatus",
    "Assets",
    "AssetPairs",
    "Ticker",
    "OHLC",
    "Depth",
    "Trades",
    "Spread",
];

const PRIVATE_METHODS: &'static [&'static str] = &[
    // User Data
    "Balance",
    "TradeBalance",
    "OpenOrders",
    "ClosedOrders",
    "QueryOrders",
    "TradesHistory",
    "QueryTrades",
    "OpenPositions",
    "Ledgers",
    "QueryLedgers",
    "TradeVolume",
    "AddExport",
    "ExportStatus",
    "RetrieveExport",
    "RemoveExport",

    // User Trading
    "AddOrder",
    "CancelOrder",
    "CancellAll",
    "CancelAllOrdersAfter",

    // User Funding
    "DepositMethods",
    "DepositAddresses",
    "DepositStatus",
    "WithdrawInfo",
    "Withdraw",
    "WithdrawStatus",
    "WithdrawCancel",
    "WalletTransfer",

    // Websocket Auth
    "GetWebSocketsToken",
];

pub fn method_type(method: &str) -> &str {
    if PRIVATE_METHODS.contains(&method) {
        "private"
    } else if PUBLIC_METHODS.contains(&method) {
        "public"
    } else {
        "invalid"
    }
}
