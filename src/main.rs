use async_kraken::client::KrakenClient;

fn get_keys() -> (String, String) {
    let content = std::fs::read_to_string("key").expect("File not found");
    let lines: Vec<&str> = content.lines().collect();

    let key = String::from(lines[0]);
    let secret = String::from(lines[1]);

    (key, secret)
}

#[async_std::main]
async fn main() {
    // # Only public endpoints
    // let k = KrakenClient::new();

    // # Public and private enpoints
    let (key, secret) = get_keys();
    let k = KrakenClient::with_credentials(key, secret);

    match k.api_request("Time", serde_json::json!({})).await {
        Ok(json) => println!("{:?}", json),
        Err(e) => println!("{:?}", e),
    };

    match k.api_request("OHLC", serde_json::json!({"pair":"doteur", "interval":30, "since":0})).await
    {
        Ok(json) => println!("{:?}", json),
        Err(e) => println!("{:?}", e),
    };

    match k.api_request("Balance", serde_json::json!({})).await {
        Ok(json) => println!("{:?}", json),
        Err(e) => println!("{:?}", e),
    };
}
